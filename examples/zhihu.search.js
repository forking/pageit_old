/**
 * HTML scraper for zhihu search
 */

const pageit = require('../lib').default

let q = 'kindle'

pageit({
  name: 'zhihu.search',
  startUrls: [`https://www.zhihu.com/search?q=${q}`],

  parseHtml: {
    selectors: ['.contents .title', [{
      title: 'a',
      link: 'a@href'
    }]]
  }
})
