/**
 * JSON scraper for json test
 */

const pageit = require('../lib').default

pageit({
  name: 'json.test',
  startUrls: ['http://jsonplaceholder.typicode.com/posts/1']
})
