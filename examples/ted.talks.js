/**
 * HTML scraper for ted talks
 */

const pageit = require('../lib').default

pageit({
  name: 'ted.talks',
  startUrls: ['https://www.ted.com/talks?language=zh-cn&page=58&sort=popular'],

  parseHtml: {
    filters: {
      toId: function (value) {
        return value.match(/\/talks\/([^\?]+)\?*/)[1]
      }
    },
    selectors: ['#browse-results .talk-link', [{
      id: '.media__message .m5 a@href | toId',
      img: '.media__image .thumb__image@src',
      duration: '.media__image .thumb__duration | trim',
      speaker: '.media__message .talk-link__speaker',
      title: '.media__message .m5 a | trim',
      views: '.meta__item .meta__val | trim',
      rated: '.meta__row .meta__val | trim'
    }]]
  },
  next: function ($) {
    let nextUrl = $('.pagination__next').attr('href')
    return nextUrl ? `https://www.ted.com${nextUrl}` : ''
  }
})
