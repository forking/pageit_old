import cheerio from 'cheerio'
import X from 'x-ray'
import sleep from 'sleep'
import isUrl from 'is-url'
import axios from 'axios'
import axiosRetry from 'axios-retry'
import querystring from 'urlite/querystring'
import objectGet from 'lodash.get'
import isObject from 'lodash.isobject'
import defaultsDeep from 'lodash.defaultsdeep'
import randomUseragent from 'random-useragent'
import ora from 'ora'
import lowdb from 'lowdb'
import replaceAll from 'replaceall'

// TODO: scrape dynamic content generate by js
// A test scrape: for test api
// mutiprocess, throw more than one url per request

let request
let spinner
let url
let urls = []
let opts
let optsDefault = {
  name: 'pageit',
  startUrls: null, // Array || String
  interval: 100, // ms
  parseHtml: {
    filters: {
      trim: value => typeof value === 'string' ? value.trim() : value,
      reverse: value => typeof value === 'string' ? value.split('').reverse().join('') : value,
      slice: (value, start, end) => typeof value === 'string' ? value.slice(start, end) : value
    },
    selectors: [] // [scope] or [scope, selector]
  },
  fetchConfig: {
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/53.0.2785.143 Chrome/53.0.2785.143 Safari/537.36'
    },
    timeout: 2000,
    method: 'get'
  },
  randomUserAgent: false,

  nextUrl: null,
  fetch: Fetch,
  beforeParse: BeforeParse,
  parse: Parse,
  next: Next,
  extract: Extract,
  save: Save,
  done: Done,

  handleRequestError: null
}

function PageIt (config = {}) {
  initOptions(config)
  initRequest()
  Scrape()
}

function initOptions (config) {
  opts = defaultsDeep(config, optsDefault)
  if (Array.isArray(opts.startUrls)) {
    urls = opts.startUrls
  } else {
    urls.push(opts.startUrls)
  }
  opts.interval = opts.interval * 1000
  if (opts.randomUserAgent) {
    opts.fetchConfig.headers['User-Agent'] = randomUseragent.getRandom()
  }
}

function initRequest () {
  request = axios.create(opts.fetchConfig)
  axiosRetry(request, { retries: 3 })
}

function Scrape () {
  if (urls.length === 0) {
    if (opts.done) {
      return opts.done()
    }
    return ''
  }
  // console.log('spinner:', spinner)
  spinner = ora('Scraping...').start()
  url = urls.pop()
  if (!isUrl(url)) {
    spinner.text = `Invalid url: ${url}`
    spinner.fail()
    return Scrape()
  }
  // sleep.usleep(opts.interval)
  start(url)
    .then(res => res.data)
    .then(data => opts.beforeParse(data))
    .then(data => opts.parse(data))
    .then(data => opts.extract(data))
    .then(data => {
      if (!data) {
        throw new ExtractException('Extract wrong...')
      }
      opts.save(data)
      spinner.text = url
      spinner.succeed()
    })
    .catch(err => {
      spinner.text = `${err.message || err}: ${url}`
      spinner.fail()
      if (err.response && opts.handleRequestError) {
        opts.handleRequestError(err)
      }
    })
    .then(() => Scrape())
}

// PageIt flow methods
function start (url) {
  return new Promise((resolve, reject) => resolve(opts.fetch(url)))
}

function Fetch (url) {
  spinner.text = 'Fetching...'
  if (opts.fetchConfig.method === 'post') {
    opts.fetchConfig.data = querystring.parse(url.split('?')[1])
  }
  return request(url)
}

// parser
function BeforeParse (content) {
  return content
}

function Parse (content) {
  spinner.text = 'Parsing...'
  let obj
  if (isObject(content)) {
    obj = parseJson(content)
  } else if (content[0] === '{' || content[0] === '[') {
    try {
      obj = parseJson(JSON.parse(content))
    } catch(e) {
      // just reutrn as string
    }
  } else if (content[0] === '<') {
    obj = parseHtml(content)
  } else {
    // just reutrn as string
  }
  return obj ? obj : content
}

function parseHtml (html) {
  let selectors = opts.parseHtml.selectors
  let x = X({
    filters: opts.parseHtml.filters
  })
  if (!Array.isArray(selectors)) {
    selectors = [selectors, '']
  }
  return new Promise(function (resolve, reject) {
    x(html, selectors[0], selectors[1] || '')(function (err, res) {
      if (err) {
        reject(err)
      }

      // extract next match url for pagination
      if (opts.next) {
        let $ = cheerio.load(html)
        if (opts.next instanceof Function) {
          try {
            opts.nextUrl = opts.next($)
          } catch (e) {
            opts.nextUrl = ''
          }
        } else if (typeof opts.next === 'string') {
          opts.nextUrl = $(opts.next).attr('href')
        }
        updateNext(opts.nextUrl)
      }
      resolve(res)
    })
  })
}

function parseJson (json) {
  // TODO: extract json with css selectors
  // extract next match url for pagination
  if (opts.next) {
    if (opts.next instanceof Function) {
      try {
        opts.nextUrl = opts.next(json)
      } catch (e) {
        opts.nextUrl = ''
      }
    } else if (typeof opts.next === 'string') {
      opts.nextUrl = objectGet(json, opts.next, '')
    }
    updateNext(opts.nextUrl)
  }
  return json
}

function Extract (obj) {
  // extract data and return to save
  // used for user to override
  return obj
}

function Save (obj) {
  const db = lowdb(`${opts.name}.json`)
  db.set(replaceAll('.', ',', url), obj).value()
}

/**
 * Next - pagination
 * @param {object|$} nextContent object for html, $ for json
 * this function can be overided to a selector string
 * notice: last page or test should return ''
 */
function Next (nextContent) {
  return ''
}

function updateNext (url) {
  if (!url) return

  if (Array.isArray(url)) {
    url.forEach(function (link) {
      updateNext(link)
    })
    return
  }

  if (!isUrl(url)) {
    if (url.length > 0) {
      spinner.text = `Invalid url: ${url}`
    }
    return
  }

  urls.push(url)
}

function Done () {}

function ExtractException (message) {
  this.message = message
  this.name = 'ExtractException'
}

export default PageIt
