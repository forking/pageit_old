'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

var _xRay = require('x-ray');

var _xRay2 = _interopRequireDefault(_xRay);

var _sleep = require('sleep');

var _sleep2 = _interopRequireDefault(_sleep);

var _isUrl = require('is-url');

var _isUrl2 = _interopRequireDefault(_isUrl);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _axiosRetry = require('axios-retry');

var _axiosRetry2 = _interopRequireDefault(_axiosRetry);

var _querystring = require('urlite/querystring');

var _querystring2 = _interopRequireDefault(_querystring);

var _lodash = require('lodash.get');

var _lodash2 = _interopRequireDefault(_lodash);

var _lodash3 = require('lodash.isobject');

var _lodash4 = _interopRequireDefault(_lodash3);

var _lodash5 = require('lodash.defaultsdeep');

var _lodash6 = _interopRequireDefault(_lodash5);

var _randomUseragent = require('random-useragent');

var _randomUseragent2 = _interopRequireDefault(_randomUseragent);

var _ora = require('ora');

var _ora2 = _interopRequireDefault(_ora);

var _lowdb = require('lowdb');

var _lowdb2 = _interopRequireDefault(_lowdb);

var _replaceall = require('replaceall');

var _replaceall2 = _interopRequireDefault(_replaceall);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// TODO: scrape dynamic content generate by js
// A test scrape: for test api
// mutiprocess, throw more than one url per request

var request = void 0;
var spinner = void 0;
var url = void 0;
var urls = [];
var opts = void 0;
var optsDefault = {
  name: 'pageit',
  startUrls: null, // Array || String
  interval: 100, // ms
  parseHtml: {
    filters: {
      trim: function trim(value) {
        return typeof value === 'string' ? value.trim() : value;
      },
      reverse: function reverse(value) {
        return typeof value === 'string' ? value.split('').reverse().join('') : value;
      },
      slice: function slice(value, start, end) {
        return typeof value === 'string' ? value.slice(start, end) : value;
      }
    },
    selectors: [] // [scope] or [scope, selector]
  },
  fetchConfig: {
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/53.0.2785.143 Chrome/53.0.2785.143 Safari/537.36'
    },
    timeout: 2000,
    method: 'get'
  },
  randomUserAgent: false,

  nextUrl: null,
  fetch: Fetch,
  beforeParse: BeforeParse,
  parse: Parse,
  next: Next,
  extract: Extract,
  save: Save,
  done: Done,

  handleRequestError: null
};

function PageIt() {
  var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  initOptions(config);
  initRequest();
  Scrape();
}

function initOptions(config) {
  opts = (0, _lodash6.default)(config, optsDefault);
  if (Array.isArray(opts.startUrls)) {
    urls = opts.startUrls;
  } else {
    urls.push(opts.startUrls);
  }
  opts.interval = opts.interval * 1000;
  if (opts.randomUserAgent) {
    opts.fetchConfig.headers['User-Agent'] = _randomUseragent2.default.getRandom();
  }
}

function initRequest() {
  request = _axios2.default.create(opts.fetchConfig);
  (0, _axiosRetry2.default)(request, { retries: 3 });
}

function Scrape() {
  if (urls.length === 0) {
    if (opts.done) {
      return opts.done();
    }
    return '';
  }
  // console.log('spinner:', spinner)
  spinner = (0, _ora2.default)('Scraping...').start();
  url = urls.pop();
  if (!(0, _isUrl2.default)(url)) {
    spinner.text = 'Invalid url: ' + url;
    spinner.fail();
    return Scrape();
  }
  // sleep.usleep(opts.interval)
  start(url).then(function (res) {
    return res.data;
  }).then(function (data) {
    return opts.beforeParse(data);
  }).then(function (data) {
    return opts.parse(data);
  }).then(function (data) {
    return opts.extract(data);
  }).then(function (data) {
    if (!data) {
      throw new ExtractException('Extract wrong...');
    }
    opts.save(data);
    spinner.text = url;
    spinner.succeed();
  }).catch(function (err) {
    spinner.text = (err.message || err) + ': ' + url;
    spinner.fail();
    if (err.response && opts.handleRequestError) {
      opts.handleRequestError(err);
    }
  }).then(function () {
    return Scrape();
  });
}

// PageIt flow methods
function start(url) {
  return new Promise(function (resolve, reject) {
    return resolve(opts.fetch(url));
  });
}

function Fetch(url) {
  spinner.text = 'Fetching...';
  if (opts.fetchConfig.method === 'post') {
    opts.fetchConfig.data = _querystring2.default.parse(url.split('?')[1]);
  }
  return request(url);
}

// parser
function BeforeParse(content) {
  return content;
}

function Parse(content) {
  spinner.text = 'Parsing...';
  var obj = void 0;
  if ((0, _lodash4.default)(content)) {
    obj = parseJson(content);
  } else if (content[0] === '{' || content[0] === '[') {
    try {
      obj = parseJson(JSON.parse(content));
    } catch (e) {
      // just reutrn as string
    }
  } else if (content[0] === '<') {
    obj = parseHtml(content);
  } else {
    // just reutrn as string
  }
  return obj ? obj : content;
}

function parseHtml(html) {
  var selectors = opts.parseHtml.selectors;
  var x = (0, _xRay2.default)({
    filters: opts.parseHtml.filters
  });
  if (!Array.isArray(selectors)) {
    selectors = [selectors, ''];
  }
  return new Promise(function (resolve, reject) {
    x(html, selectors[0], selectors[1] || '')(function (err, res) {
      if (err) {
        reject(err);
      }

      // extract next match url for pagination
      if (opts.next) {
        var $ = _cheerio2.default.load(html);
        if (opts.next instanceof Function) {
          try {
            opts.nextUrl = opts.next($);
          } catch (e) {
            opts.nextUrl = '';
          }
        } else if (typeof opts.next === 'string') {
          opts.nextUrl = $(opts.next).attr('href');
        }
        updateNext(opts.nextUrl);
      }
      resolve(res);
    });
  });
}

function parseJson(json) {
  // TODO: extract json with css selectors
  // extract next match url for pagination
  if (opts.next) {
    if (opts.next instanceof Function) {
      try {
        opts.nextUrl = opts.next(json);
      } catch (e) {
        opts.nextUrl = '';
      }
    } else if (typeof opts.next === 'string') {
      opts.nextUrl = (0, _lodash2.default)(json, opts.next, '');
    }
    updateNext(opts.nextUrl);
  }
  return json;
}

function Extract(obj) {
  // extract data and return to save
  // used for user to override
  return obj;
}

function Save(obj) {
  var db = (0, _lowdb2.default)(opts.name + '.json');
  db.set((0, _replaceall2.default)('.', ',', url), obj).value();
}

/**
 * Next - pagination
 * @param {object|$} nextContent object for html, $ for json
 * this function can be overided to a selector string
 * notice: last page or test should return ''
 */
function Next(nextContent) {
  return '';
}

function updateNext(url) {
  if (!url) return;

  if (Array.isArray(url)) {
    url.forEach(function (link) {
      updateNext(link);
    });
    return;
  }

  if (!(0, _isUrl2.default)(url)) {
    if (url.length > 0) {
      spinner.text = 'Invalid url: ' + url;
    }
    return;
  }

  urls.push(url);
}

function Done() {}

function ExtractException(message) {
  this.message = message;
  this.name = 'ExtractException';
}

exports.default = PageIt;